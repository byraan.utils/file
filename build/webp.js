// file
const fs=require('fs'),
path=require('path'),
find=require('find');
// custom global
const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
regexp=require('../lib/regexp'),
exec=require('../lib/exec');
cli: {
	commandLineArgs(this, [
		{
			name: 'input',
			alias: 'i',
			type: String,
			defaultValue: ['.'],
			multiple: true
		}, {
			name: 'clean',
			alias: 'c',
			type: Boolean
		}, {
			name: 'to',
			type: String,
			defaultValue: 'jpg'
		}
	], 2);
	process.arguments.to=this.subcommands[0];
	/* process.arguments.input_alt=this.subcommands[1];
	if(fs.existsSync(process.arguments.input_alt)) process.arguments.input=[process.arguments.input_alt]; */
}
if(process.arguments.to){
	if(process.arguments.clean){
		for(let i=0; i<process.arguments.input.length; i++){
			process.arguments.input[i]=path.resolve(process.arguments.input[i]);
			let fileList=find.fileSync(regexp.file(process.arguments.input[i], '', [process.arguments.to]), process.arguments.input[i]);
			if(fileList.length==0 && fs.existsSync(process.arguments.input[i])) fileList=[process.arguments.input[i]];
			for(let j=0; j<fileList.length; j++){
				let data=path.parse(fileList[j]);
				let file_original=`${data.dir}/${data.name}.webp`;
				if(fs.existsSync(file_original)) fs.unlinkSync(file_original);
			}
		}
	}
	else{
		let lib=path.resolve(`${__dirname}/../resources/libwebp-1.0.2-mac-10.14`);
		let exec_webp=[];
		for(let i=0; i<process.arguments.input.length; i++){
			process.arguments.input[i]=path.resolve(process.arguments.input[i]);
			let fileList=find.fileSync(regexp.file(process.arguments.input[i], '', ['webp']), process.arguments.input[i]);
			if(fileList.length==0 && fs.existsSync(process.arguments.input[i])) fileList=[process.arguments.input[i]];
			for(let j=0; j<fileList.length; j++){
				let data=path.parse(fileList[j]);
				if(data.base.substr(0, 1)=='.') continue;
				exec_webp.push(`"${lib}/bin/dwebp" "${fileList[j]}" -o "${data.dir}/${data.name}.${process.arguments.to}"`);
			}
		}
		if(exec_webp.length) exec.apply('', exec_webp);
	}
}
else{
	log('{{error}} Invalid format');
}